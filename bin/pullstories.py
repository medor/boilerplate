#! /usr/bin/env python3


# Copyright (C) 2015-2017 Alexandre Leray

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Pulls stories from our plateform
#
# Usage:
#
#     ./pullstories.py [where]


import requests
import settings
import os


def get_client():
    login_url = 'https://example.org/fr/admin/login/'
    client = requests.session()
    # Retrieve the CSRF token first
    client.get(login_url, verify=False)  # sets cookie
    csrftoken = client.cookies['csrftoken']
    login_data = dict(username=settings.USER, password=settings.PASS, csrfmiddlewaretoken=csrftoken, next='/')
    resp = client.post(login_url, data=login_data, headers=dict(Referer=login_url))

    if resp.status_code != 200:
        import sys
        sys.exit()

    return client


def pull_stories(where="stories"):
    client = get_client()

    hostname = 'https://example.org'
    path = "the/path/to/the/api"
    url = hostname + path

    request = client.get(url)
    #  request = requests.get(url, headers={'Authorization': 'Token {}'.format(settings.TOKEN)})

    for membership in request.json()["articlemembership_set"]:
        if membership["article"]:
            name = membership['slug'] if membership['slug'] else membership['article']['slug']

            print("Pulling %s" % name)

            # Gets the story
            # # TODO: namespace the url on the django project
            request = client.get(hostname + "/fr/%s.html" % membership['id'])
            local_path = os.path.join(where, name)
            if not os.path.exists(local_path):
                os.makedirs(local_path)
            f = open("%s/%s/index.html" % (where, name), "w")
            f.write(request.text)
            f.close()

            # Gets the css of the story
            # # TODO: namespace the url on the django project
            request = client.get(hostname + "/fr/%s.css" % membership['id'])
            f = open("%s/%s/styles.css" % (where, name), "w")
            f.write(request.text)
            f.close()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('where', default="stories")
    args = parser.parse_args()

    pull_stories(where=args.where)
